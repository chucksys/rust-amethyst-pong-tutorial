use amethyst::{
    core::Transform,
    ecs::prelude::{Join, ReadStorage, System, WriteStorage}
};

use crate::pong::ARENA_HEIGHT;
use crate::components::*;

pub struct BounceSystem;

impl<'s> System<'s> for BounceSystem {
    type SystemData = (
        WriteStorage<'s, Ball>,
        ReadStorage<'s, Paddle>,
        ReadStorage<'s, Transform>
    );

    fn run(&mut self, (mut balls, paddles, transforms): Self::SystemData) {
        for (ball, transform) in (&mut balls, &transforms).join() {
            if ball_collides_ywalls(&transform, &ball) {
                    ball.velocity[1] = -ball.velocity[1];
            }

            for (paddle, paddle_trans) in (&paddles, &transforms).join() {
                if ball_collides_paddle(&transform, &paddle_trans, &paddle, &ball) {
                    ball.velocity[0] = -ball.velocity[0];
                }

            }
        }
    }
}

fn ball_collides_ywalls(transform: &Transform, ball: &Ball) -> bool {
    let ball_y = transform.translation().y;

    (ball_y <= ball.radius && ball.velocity[1] < 0.)
        || (ball_y >= ARENA_HEIGHT - ball.radius && ball.velocity[1] > 0.)
}

fn ball_collides_paddle(ball_transform: &Transform, paddle_transform: &Transform, paddle: &Paddle, ball: &Ball) -> bool {

    let ball_x = ball_transform.translation().x;
    let ball_y = ball_transform.translation().y;
    let paddle_x = paddle_transform.translation().x - (paddle.width * 0.5);
    let paddle_y = paddle_transform.translation().y - (paddle.height * 0.5);

    let in_rect = point_in_rect(
        ball_x, ball_y,
        paddle_x - ball.radius, paddle_y - ball.radius,
        paddle_x + paddle.width + ball.radius, paddle_y + paddle.height + ball.radius);
    let has_correct_velocity = (paddle.side == Side::Left && ball.velocity[0] < 0.)
        || (paddle.side == Side::Right && ball.velocity[0] > 0.);

    in_rect && has_correct_velocity
}

fn point_in_rect(x: f32, y: f32, left: f32, bottom: f32, right: f32, top: f32) -> bool {
    x >= left && x <= right && y >= bottom && y <= top
}
