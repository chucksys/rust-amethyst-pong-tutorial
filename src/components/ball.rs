use amethyst::{
    assets::Handle,
    core::transform::Transform,
    ecs::prelude::{Component, DenseVecStorage},
    prelude::*,
    renderer::{SpriteRender, SpriteSheet},
};
use crate::pong::*;

pub struct Ball {
    pub velocity: [f32; 2],
    pub radius: f32
}

impl Ball {
    fn new() -> Self {
        Ball {
            radius: BALL_RADIUS,
            velocity: [BALL_VELOCITY_X, BALL_VELOCITY_Y]
        }
    }
}

impl Component for Ball {
    type Storage = DenseVecStorage<Self>;
}

pub fn initialize_ball(world: &mut World, sprite_sheet_handle: Handle<SpriteSheet>) {
    let mut local_transform = Transform::default();
    local_transform.set_translation_xyz(ARENA_WIDTH / 2., ARENA_HEIGHT / 2., 0.);

    let sprite_render = SpriteRender {
        sprite_sheet: sprite_sheet_handle,
        sprite_number: 1
    };

    world
        .create_entity()
        .with(sprite_render)
        .with(Ball::new())
        .with(local_transform)
        .build();
}
