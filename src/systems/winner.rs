use amethyst::{
    core::transform::Transform,
    core::SystemDesc,
    derive::SystemDesc,
    ecs::prelude::{Join, System, SystemData, Write, ReadExpect, World, WriteStorage},
    ui::{Anchor, TtfFormat, UiText, UiTransform},
};

use crate::components::Ball;
use crate::components::Side;
use crate::components::{ScoreBoard, ScoreText};
use crate::pong::ARENA_WIDTH;

#[derive(SystemDesc)]
pub struct WinnerSystem;

impl<'s> System<'s> for WinnerSystem {
    type SystemData = (
        WriteStorage<'s, Ball>,
        WriteStorage<'s, Transform>,
        WriteStorage<'s, UiText>,
        Write<'s, ScoreBoard>,          // Not a collection, but a singleton
        ReadExpect<'s, ScoreText>,      // Like Read, but fails if not found
    );

    fn run(&mut self, (mut balls, mut transforms, mut ui, mut board, text): Self::SystemData) {
        for (ball, transform) in (&mut balls, &mut transforms).join() {
            if let Some(side) = ball_at_side(&transform, &ball) {
                ball.velocity[0] = -ball.velocity[0];
                transform.set_translation_x(ARENA_WIDTH / 2.);

                match side {
                    Side::Left => board.score_right += 1,
                    Side::Right => board.score_left += 1,
                }

                if let Some(text) = ui.get_mut(text.p1_score) {
                    text.text = board.score_left.to_string();
                }
                if let Some(text) = ui.get_mut(text.p2_score) {
                    text.text = board.score_right.to_string();
                }
            }
        }
    }
}

fn ball_at_side(transform: &Transform, ball: &Ball) -> Option<Side> {
    let ball_x = transform.translation().x;

    if ball_x <= ball.radius {
        Some(Side::Left)
    } else if ball_x >= ARENA_WIDTH - ball.radius {
        Some(Side::Right)
    } else {
        None
    }
}
