use amethyst::{
    assets::{AssetStorage, Loader, Handle},
    prelude::*,
    ecs::prelude::{Component, DenseVecStorage, Entity},
    ui::{Anchor, TtfFormat, UiText, UiTransform},
};

#[derive(Default)]
pub struct ScoreBoard {
    pub score_left: u32,
    pub score_right: u32
}

pub struct ScoreText {
    pub p1_score: Entity,
    pub p2_score: Entity,
}

pub fn initialize_scoreboard(world: &mut World) {
    let font = world.read_resource::<Loader>()
        .load("fonts/square.ttf", TtfFormat, (), &world.read_resource());
    let p1_trans = UiTransform::new(
        "P1".to_string(), Anchor::TopMiddle, Anchor::TopMiddle,
        -50., -50., 1., 200., 50.);
    let p2_trans = UiTransform::new(
        "P2".to_string(), Anchor::TopMiddle, Anchor::TopMiddle,
        50., -50., 1., 200., 50.);

    let p1_score = world.create_entity()
        .with(p1_trans)
        .with(UiText::new(font.clone(), "0".to_string(), [1., 1., 1., 1.], 50.))
        .build();
    let p2_score = world.create_entity()
        .with(p2_trans)
        .with(UiText::new(font.clone(), "0".to_string(), [1., 1., 1., 1.], 50.))
        .build();

    world.insert(ScoreText {p1_score, p2_score});
}
